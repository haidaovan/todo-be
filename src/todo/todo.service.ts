import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { PrismaService } from '../providers/prisma/prisma.service';

@Injectable()
export class TodoService {
  constructor(private prisma: PrismaService) {}

  async create(createTodoDto: CreateTodoDto) {
    return await this.prisma.todo.create({
      data: createTodoDto,
    });
  }

  async findAll(completed) {
    if (!completed) {
      return await this.prisma.todo.findMany();
    } else {
      return await this.prisma.todo.findMany({
        where: { completed: JSON.parse(completed) },
      });
    }
  }

  async findOne(id: number) {
    return await this.prisma.todo.findUniqueOrThrow({ where: { id: id } });
  }

  async update(id: number, updateTodoDto: UpdateTodoDto) {
    const todo = await this.prisma.todo.findUniqueOrThrow({
      where: { id: id },
    });
    if (!todo) {
      throw new NotFoundException('to do not found');
    }
    return await this.prisma.todo.update({
      where: { id: id },
      data: updateTodoDto,
    });
  }

  async remove(id: number) {
    await this.prisma.todo.delete({
      where: { id: id },
    });
    return { status: true };
  }
}
